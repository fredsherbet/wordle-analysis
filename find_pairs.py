#!/usr/bin/python3

import logging
import argparse
import itertools
from datetime import datetime

GAMES_PLAYED = 0


def find_unique_pairs_for_answer(words, answers, answer):
    # Determine which pairs of guesses pinpoint the answer.
    # An answer is pinpointed by a guess, if the only possible word is the
    # answer, based on the clues given by using those 2 words.
    #
    # So, given the answer is the answer to the game, are any wrong guesses
    # still available to try?
    game = Game(words, answer)

    # If the guess includes the answer, then clearly the guess pinpoints the
    # answer, so reduce the processing by skipping that word. That does change
    # the output, though - further processing needs to keep that in mind.
    words_to_try = [w for w in words if w != answer]
    logging.info('Got %d words to try', len(words_to_try))

    for guesses in itertools.combinations(words_to_try, 2):
        # A guess is a good one, if it eliminates all words except the answer.
        if any(possible_answer != answer
               for possible_answer 
               in possible_answers(game, answers, guesses)):
            logging.debug('Found bad guess %s for %s', guesses, answer)
        else:
            logging.info('Found good guess %s for %s', guesses, answer)
            yield guesses


def possible_answers(game, answers, guesses):
    # The answer is still possible if the guesses don't eliminate it.
    # For example, when the answer is "abort", and a guess is "abbey",
    # the a & b are green; the rest are black.
    # We are left with all words that begin "ab", and don't have e, y or a
    # second b.

    # We have already stripped out the answer from the guesses; we also know
    # that the words in the guesses will be eliminated.
    return (a for a in answers
            if a != game.answer and
            a not in guesses and
            not game.is_word_eliminated(a, guesses))


class Game:
    def __init__(self, words, answer):
        self.words = words
        self.answer = answer

    def is_word_eliminated(self, word, guesses):
        global GAMES_PLAYED
        GAMES_PLAYED += 1
        logging.debug("Is %s eliminated by %s when answer is %s?",
                      word, guesses, self.answer)
        return any(self.guess_eliminates_word(word, guess)
                   for guess in guesses)

    def guess_eliminates_word(self, word, guess):

        if self.green_tile_eliminates_word(word, guess):
            logging.debug("Green in %s eliminates %s when answer is %s",
                          guess, word, self.answer)
            return True

        # Handle yellow and black tiles
        for letter in guess:
            guess_count = sum(1 for l in guess if l == letter)
            answer_count = sum(1 for l in self.answer if l == letter)
            word_count = sum(1 for l in word if l == letter)
            if guess_count > answer_count:
                logging.debug('Guess %s reveals that the answer %s has '
                              'exactly %d of letter %s', 
                              guess, self.answer, answer_count, letter)
                if word_count != answer_count:
                    logging.debug('Count of letter %s eliminates %s when '
                                  'answer is %s',
                                  letter, word, self.answer)
                    return True
            else:
                # guess_count <= answer_count
                logging.debug('Guess %s reveals that the answer has at least '
                              '%d of letter %s',
                              guess, guess_count, letter)
                if word_count < guess_count:
                    logging.debug('Count of letter %s eliminates %s when '
                                  'answer is %s',
                                  letter, word, self.answer)
                    return True
            
        logging.debug("%s is not eliminated by guess %s when answer is %s",
                      word, guess, self.answer)
        return False

    def green_tile_eliminates_word(self, word, guess):
        for (g_l, w_l, a_l) in zip(guess, word, self.answer):
            if g_l == a_l:
                # Tile is green
                if w_l != a_l:
                    # But wouldn't be if word could still be an answer
                    return True
        return False


def parse_args():
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("-l", "--startswith", required=False, 
                             default=None,
                             help="Only handle answers starting with the "
                             "given letter")
    args_parser.add_argument("-n", "--number", required=False, default=0,
                             help="Only look at first number of words")
    args_parser.add_argument("-c", "--count", required=False, default=0,
                             help="Calculate count answers")
    args_parser.add_argument("-s", "--startingfrom", required=False, 
                             default=0,
                             help="Skip answers to calculate")
    args_parser.add_argument("-w", "--word", required=False, default=None,
                             help="Word to calculate answers for")
    args_parser.add_argument("-p", "--perf", required=False, default=0,
                             help="Reduce answer set for perf comparison")
    return vars(args_parser.parse_args())


if __name__ == '__main__':
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)

    args = parse_args()

    with open('wordle-answers.txt') as f:
        words = [l.strip() for l in f.readlines()]
    answers = words
    answers_to_process = answers

    if args['number']:
        words = words[:args['number']]

    if args['startswith']:
        answers_to_process = [w for w in answers 
                              if w.startswith(args['startswith'])]

    if args['count'] and args['startingfrom']:
        answers_to_process = answers_to_process[
            int(args['startingfrom']):
            int(args['startingfrom']) + int(args['count'])]

    if args['word']:
        answers_to_process = [args['word']]

    if args['perf']:
        words = words[:int(args['perf'])]
        answers = words
        answers_to_process = words

    logging.info("Got %d words, and calculating pairs for %d answers", 
                 len(words), len(answers_to_process))
    started = datetime.now()
    for word in answers_to_process:
        with open('pairs/{}'.format(word), 'w') as f:
            for good_guess in find_unique_pairs_for_answer(words, answers, word):
                f.write(','.join(good_guess))
                f.write('\n')
        logging.info("%s games played in %s seconds", 
                     GAMES_PLAYED, (datetime.now() - started).seconds)
